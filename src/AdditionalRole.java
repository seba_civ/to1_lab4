
public enum AdditionalRole {
    GROUP_REPRESENTATIVE(50),
    YEAR_REPRESENTATIVE(100),
    STUDENT_COUNCIL_MEMBER(150);

    private int scholarship; // int ??

    AdditionalRole(int scholarship) {
        this.scholarship = scholarship;
    }

    public int getScholarship() {
        return scholarship;
    }

    @Override
    public String toString() {
        return "some nice description";
    }
}
