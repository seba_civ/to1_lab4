import java.util.EnumSet;

public class Student {
    private final String firstName;
    private final String lastName;
    private final EnumSet<AdditionalRole> roles;

    public Student(String firstName, String lastName, EnumSet<AdditionalRole> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.roles = EnumSet.copyOf(roles);
    }

    public boolean addRole(AdditionalRole role) {
        return roles.add(role);
    }

    public boolean removeRole(AdditionalRole role) {
        return roles.remove(role);
    }

    public EnumSet<AdditionalRole> getRoles() {
        return EnumSet.copyOf(roles);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
